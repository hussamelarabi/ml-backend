Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :statistics
      resources :predictions do
        collection do
          get 'train', action: :train_model
          get 'predict', action: :predict
          # post ":id/add_message", action: :add_message
          # get "trips"
          # get "owner_bookings"
          # get "pendings", action: :pendings
          # get "current", action: :current_bookings
          # get "history", action: :booking_history
        end
      end
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

class CreateStatistics < ActiveRecord::Migration[5.2]
  def change
    create_table :statistics do |t|
      t.datetime :last_train_attempt
      t.bigint :dataset_size
      t.bigint :pop_count
      t.bigint :rock_count
      t.bigint :instrumental_count
      t.bigint :hiphop_count
      t.float :accuracy
      t.float :precision
      t.float :recall

      t.timestamps
    end
  end
end

module Api
  module V1
    class StatisticsController < ApplicationController
      def index
        @statistics = Statistic.last
        render json: { status: 'SUCCESS', message: 'Loaded statistics', data: @statistics }, status: :ok
      end
    end
  end
end
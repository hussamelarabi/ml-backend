module Api
  module V1
    class PredictionsController < ApplicationController
      def index; end

      def train_model
        test = ` python3 Model/Trainer.py`
        puts test
      end

      def predict
        puts 'Download file'
        `cd ~/RubymineProjects/ml-backend/Model/'User Audio'/Unprocessed/ && curl -O -J #{params['0']}`
        puts 'Crop to size'
        `cd ~/RubymineProjects/ml-backend/Model/'User Audio'/Unprocessed/ && ffmpeg -ss 60 -i * -t 30 -c copy ../Processed/out.mp3`
        puts 'Delete original file after crop'
        `cd ~/RubymineProjects/ml-backend/Model/'User Audio'/Unprocessed/ && rm *`
        puts 'Extract features from 30 second sample'
        `python3 ~/RubymineProjects/ml-backend/Model/featuresExtractor.py`
        puts 'Delete audio Sample'
        `cd ~/RubymineProjects/ml-backend/Model/'User Audio'/Processed/ && rm *`
        puts '6'
        result = `python3 ~/RubymineProjects/ml-backend/Model/predictor.py`
        puts 'prediction result: ', result
        render json: { status: 'SUCCESS', message: result },
               status: :ok
      end

    end
  end
end
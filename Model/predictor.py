import numpy as np
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
import pandas as pd

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle


def forward_propagation(X, parameters):
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    W2 = parameters["W2"]
    b2 = parameters["b2"]

    Z1 = np.dot(W1, X) + b1
    A1 = np.tanh(Z1)
    Z2 = np.dot(W2, A1) + b2
    A2 = 1 / (1 + np.exp(-Z2))

    cache = {
        "Z1": Z1,
        "A1": A1,
        "Z2": Z2,
        "A2": A2}
    return A2, cache


def predict(parameters, X):
    A2, cache = forward_propagation(X, parameters)
    prediction = np.argmax(A2, axis=0)
    return prediction.reshape(1, prediction.shape[0])


def main():
    # Importing dataset
    data = pd.read_csv('/home/hussam/RubymineProjects/ml-backend/Model/data.csv')
    data = data.drop(['filename'], axis=1)
    with open('/home/hussam/RubymineProjects/ml-backend/Model/parameters.p', 'rb') as fp:
        parameters = pickle.load(fp)

    scaler = StandardScaler()
    X = scaler.fit_transform(np.array(data.iloc[:, :-1], dtype=float))

    y_pred = predict(parameters, X.T)
    y_pred = y_pred[0]

    for i in range(y_pred.size):
        if y_pred[i] == 0:
            print('hipHop')
        elif y_pred[i] == 1:
            print('instrumental')
        elif y_pred[i] == 2:
            print('pop')
        else:
            print('rock')


main()

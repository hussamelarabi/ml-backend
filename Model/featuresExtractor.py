import librosa
import numpy as np
import os
import csv
import warnings

warnings.filterwarnings('ignore')


header = 'filename chroma_stft spectral_centroid spectral_bandwidth rolloff zero_crossing_rate'
for i in range(1, 21):
    header += f' mfcc{i}'
header += ' label'
header = header.split()

file = open('/home/hussam/RubymineProjects/ml-backend/Model/data.csv', 'w', newline='')
with file:
    writer = csv.writer(file)
    writer.writerow(header)
genres = 'rock instrumental hiphop pop'.split()

for filename in os.listdir(f'/home/hussam/RubymineProjects/ml-backend/Model/User Audio/Processed'):
    songname = f'/home/hussam/RubymineProjects/ml-backend/Model/User Audio/Processed/{filename}'

    y, sr = librosa.load(songname)
    chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
    spec_cent = librosa.feature.spectral_centroid(y=y, sr=sr)
    spec_bw = librosa.feature.spectral_bandwidth(y=y, sr=sr)
    rms = librosa.feature.rms(y=y)
    rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr)
    zcr = librosa.feature.zero_crossing_rate(y)
    mfcc = librosa.feature.mfcc(y=y, sr=sr)
    to_append = f'{filename} {np.mean(chroma_stft)} {np.mean(spec_cent)} {np.mean(spec_bw)} {np.mean(rolloff)} {np.mean(zcr)}'
    for e in mfcc:
        to_append += f' {np.mean(e)}'
    to_append += f' Unknown'
    file = open('/home/hussam/RubymineProjects/ML-Backend/Model/data.csv', 'a', newline='')
    with file:
        writer = csv.writer(file)
        writer.writerow(to_append.split())
